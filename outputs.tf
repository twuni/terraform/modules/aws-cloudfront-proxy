output "dns_record" {
  description = "The domain name of this website's CloudFront distribution (CDN). Can be used to create a Route 53 alias record."
  value       = aws_cloudfront_distribution.cdn.domain_name
}

output "dns_zone" {
  description = "The ID of the Route 53 Hosted Zone for this website's CloudFront distribution (CDN). Can be used to create a Route 53 alias record."
  value       = aws_cloudfront_distribution.cdn.hosted_zone_id
}
