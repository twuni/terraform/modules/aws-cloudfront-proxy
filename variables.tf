variable "dns_zone" {
  description = "The ID of the Route 53 hosted zone to which the given proxy domain name belongs."
  type        = string
}

variable "domain" {
  description = "The domain name for the proxy."
  type        = string
}

variable "source_domain" {
  description = "The hostname of the upstream proxied website."
  type        = string
}

variable "source_path" {
  default     = "/"
  description = "The base URI path at which to anchor proxied requests."
  type        = string
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}
