module "Certificate" {
  source             = "git::https://gitlab.com/twuni/terraform/modules/aws-acm-certificate.git?ref=main"
  additional_domains = ["www.${var.domain}"]
  dns_zone           = var.dns_zone
  domain             = var.domain
}
