# CloudFront Proxy | AWS | Terraform Modules | Twuni

This Terraform module provisions a CloudFront distribution hosted at a
given domain, with TLS termination, which proxies an HTTP-only upstream
website.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.52.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_dns_zone"></a> [dns\_zone](#input\_dns\_zone) | The ID of the Route 53 hosted zone to which the given proxy domain name belongs. | `string` | n/a | yes |
| <a name="input_domain"></a> [domain](#input\_domain) | The domain name for the proxy. | `string` | n/a | yes |
| <a name="input_source_domain"></a> [source\_domain](#input\_source\_domain) | The hostname of the upstream proxied website. | `string` | n/a | yes |
| <a name="input_source_path"></a> [source\_path](#input\_source\_path) | The base URI path at which to anchor proxied requests. | `string` | `"/"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dns_record"></a> [dns\_record](#output\_dns\_record) | The domain name of this website's CloudFront distribution (CDN). Can be used to create a Route 53 alias record. |
| <a name="output_dns_zone"></a> [dns\_zone](#output\_dns\_zone) | The ID of the Route 53 Hosted Zone for this website's CloudFront distribution (CDN). Can be used to create a Route 53 alias record. |
